package com.example.bloot_mohammadhosseinranjbar.presentation.di

import com.example.bloot_mohammadhosseinranjbar.presentation.di.news.NewsSubComponent

interface Injector {
    fun createNewsSubComponent(): NewsSubComponent
}