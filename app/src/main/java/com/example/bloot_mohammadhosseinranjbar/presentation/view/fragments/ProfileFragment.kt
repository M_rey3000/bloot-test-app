package com.example.bloot_mohammadhosseinranjbar.presentation.view.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.bloot_mohammadhosseinranjbar.R
import com.example.bloot_mohammadhosseinranjbar.databinding.FragmentProfileBinding


class ProfileFragment : Fragment() {

    private var binding: FragmentProfileBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            gitText.setOnClickListener {
                browse(getString(R.string.git_lab_url))
            }
            linkedinText.setOnClickListener {
                browse(getString(R.string.linkedin_url))
            }
            btnAboutMe.setOnClickListener {
                AboutMeBottomSheetFragment().show(childFragmentManager, null)
            }
        }

    }

    private fun browse(url: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }


}