package com.example.bloot_mohammadhosseinranjbar.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.GetNewsUseCase
import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.UpdateNewsUseCase

class NewsViewModel(
    private val getNewsUseCase: GetNewsUseCase,
    private val updateNewsUseCase: UpdateNewsUseCase
) : ViewModel() {
    // Using live data block for coroutine on main thread (using IO on data sources)
    fun getNews() = liveData {
        emit(getNewsUseCase.execute())
    }

    fun updateNews() = liveData {
        emit(updateNewsUseCase.execute())
    }
}