package com.example.bloot_mohammadhosseinranjbar.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.GetNewsUseCase
import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.UpdateNewsUseCase

class NewsViewModelFactory(
    private val getNewsUseCase: GetNewsUseCase,
    private val updateNewsUseCase: UpdateNewsUseCase
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return NewsViewModel(getNewsUseCase, updateNewsUseCase) as T
    }
}