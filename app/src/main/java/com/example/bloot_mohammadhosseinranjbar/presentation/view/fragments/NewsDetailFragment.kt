package com.example.bloot_mohammadhosseinranjbar.presentation.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.bloot_mohammadhosseinranjbar.data.model.news.News
import com.example.bloot_mohammadhosseinranjbar.databinding.FragmentNewsDetailBinding


class NewsDetailFragment : Fragment() {

    private lateinit var news: News
    private lateinit var binding: FragmentNewsDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if (it.containsKey("news"))
                news = it.getParcelable("news")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentNewsDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.news = news
    }

}