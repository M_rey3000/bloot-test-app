package com.example.bloot_mohammadhosseinranjbar.presentation.di.core

import android.content.Context
import androidx.room.Room
import com.example.bloot_mohammadhosseinranjbar.data.db.news.NewsDAO
import com.example.bloot_mohammadhosseinranjbar.data.db.news.NewsDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Singleton
    @Provides
    fun provideNewsDb(context: Context): NewsDB =
        Room.databaseBuilder(
            context.applicationContext,
            NewsDB::class.java, "news_db"
        ).build()

    @Singleton
    @Provides
    fun provideNewsDao(newsDB: NewsDB): NewsDAO = newsDB.newsDAO

}