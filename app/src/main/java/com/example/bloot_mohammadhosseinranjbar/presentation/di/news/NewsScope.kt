package com.example.bloot_mohammadhosseinranjbar.presentation.di.news

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class NewsScope  // Limit view model to life cycle of view