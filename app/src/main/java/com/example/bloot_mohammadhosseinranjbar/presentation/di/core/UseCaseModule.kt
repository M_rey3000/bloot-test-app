package com.example.bloot_mohammadhosseinranjbar.presentation.di.core

import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRepository
import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.GetNewsUseCase
import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.UpdateNewsUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {
    @Singleton
    @Provides
    fun provideGetNewsUseCaseModule(newsRepository: NewsRepository): GetNewsUseCase =
        GetNewsUseCase(newsRepository)

    @Singleton
    @Provides
    fun provideUpdateNewsUseCaseModule(newsRepository: NewsRepository): UpdateNewsUseCase =
        UpdateNewsUseCase(newsRepository)
}