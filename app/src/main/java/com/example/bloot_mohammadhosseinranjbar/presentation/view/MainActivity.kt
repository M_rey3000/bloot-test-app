package com.example.bloot_mohammadhosseinranjbar.presentation.view


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.example.bloot_mohammadhosseinranjbar.R
import com.example.bloot_mohammadhosseinranjbar.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initNavigationBottom()

    }

    private fun initNavigationBottom() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.new_host_fragment) as NavHostFragment?
        NavigationUI.setupWithNavController(
            binding.bottomNavigation,
            navHostFragment!!.navController
        )
    }

}