package com.example.bloot_mohammadhosseinranjbar.presentation.di.core

import com.example.bloot_mohammadhosseinranjbar.data.repository.news.NewsRepositoryImpl
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsCacheDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsLocalDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRemoteDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideNewsRepositoryModule(
        newsRemoteDataSource: NewsRemoteDataSource,
        newsLocalDataSource: NewsLocalDataSource,
        newsCacheDataSource: NewsCacheDataSource
    ): NewsRepository =
        NewsRepositoryImpl(newsRemoteDataSource, newsLocalDataSource, newsCacheDataSource)


}