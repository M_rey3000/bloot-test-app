package com.example.bloot_mohammadhosseinranjbar.presentation

import android.app.Application
import com.example.bloot_mohammadhosseinranjbar.BuildConfig
import com.example.bloot_mohammadhosseinranjbar.presentation.di.Injector
import com.example.bloot_mohammadhosseinranjbar.presentation.di.core.*
import com.example.bloot_mohammadhosseinranjbar.presentation.di.news.NewsSubComponent

class App : Application(), Injector {
    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .netModule(NetModule(BuildConfig.BASE_URL))
            .remoteDataSourceModule(RemoteDataSourceModule())
            .build()
    }

    override fun createNewsSubComponent(): NewsSubComponent =
        appComponent.newsSubComponent().create()


}