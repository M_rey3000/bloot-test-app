package com.example.bloot_mohammadhosseinranjbar.presentation.di.news

import com.example.bloot_mohammadhosseinranjbar.presentation.view.MainActivity
import com.example.bloot_mohammadhosseinranjbar.presentation.view.fragments.NewsFragment
import dagger.Subcomponent

@NewsScope
@Subcomponent(modules = [NewsModule::class])
interface NewsSubComponent {
    fun inject(mainActivity: MainActivity)

    fun inject(newsFragment: NewsFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(): NewsSubComponent
    }
}