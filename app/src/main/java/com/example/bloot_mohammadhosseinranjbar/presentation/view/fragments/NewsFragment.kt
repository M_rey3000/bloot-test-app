package com.example.bloot_mohammadhosseinranjbar.presentation.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bloot_mohammadhosseinranjbar.databinding.FragmentNewsBinding
import com.example.bloot_mohammadhosseinranjbar.presentation.di.Injector
import com.example.bloot_mohammadhosseinranjbar.presentation.view.adapter.NewsRecycleViewAdapter
import com.example.bloot_mohammadhosseinranjbar.presentation.viewmodel.NewsViewModel
import com.example.bloot_mohammadhosseinranjbar.presentation.viewmodel.NewsViewModelFactory
import javax.inject.Inject


class NewsFragment : Fragment() {

    @Inject
    lateinit var factory: NewsViewModelFactory
    private lateinit var newsViewModel: NewsViewModel

    private lateinit var binding: FragmentNewsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (requireActivity().application as Injector).createNewsSubComponent()
            .inject(this)

        newsViewModel = ViewModelProvider(this, factory)[NewsViewModel::class.java]

        binding = FragmentNewsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycleView()
    }


    private fun initRecycleView() {
        val newsRecycleViewAdapter = NewsRecycleViewAdapter()
        binding.newsList.adapter = newsRecycleViewAdapter


        val news = newsViewModel.getNews()

        news.observe(viewLifecycleOwner, {
            //Toast.makeText(requireContext(), "Loading..." + it.size, Toast.LENGTH_SHORT).show()
            newsRecycleViewAdapter.setList(it)
        })

        newsRecycleViewAdapter.onItemClick = {
            Navigation.findNavController(binding.root)
                .navigate(NewsFragmentDirections.actionNewsFragmentToNewsDetailFragment(it))
        }


        // Using this approach instead of Paging library for pagination and its a very bad idea!
        val scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val layoutManager = binding.newsList.layoutManager as LinearLayoutManager
                val lastVisiblePosition = layoutManager.findLastVisibleItemPosition()

                if (lastVisiblePosition.plus(1)
                        .mod(NewsRecycleViewAdapter.pageSize) == 0
                ) { // Has more item to load
                    Toast.makeText(requireContext(), "Loading more", Toast.LENGTH_SHORT).show()
                    NewsRecycleViewAdapter.pageToLoad++
                    newsViewModel.getNews().observe(viewLifecycleOwner, {
                        Toast.makeText(
                            requireContext(),
                            "Loading..." + NewsRecycleViewAdapter.pageToLoad + "::" + it.size,
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.i("ONSCROLL", it.toString())
                        newsRecycleViewAdapter.setList(it)
                    })
                }
            }
        }

        //binding.newsList.addOnScrollListener(scrollListener)


    }

}