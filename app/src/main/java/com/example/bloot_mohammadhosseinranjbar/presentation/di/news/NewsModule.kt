package com.example.bloot_mohammadhosseinranjbar.presentation.di.news

import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.GetNewsUseCase
import com.example.bloot_mohammadhosseinranjbar.domain.usecase.news.UpdateNewsUseCase
import com.example.bloot_mohammadhosseinranjbar.presentation.viewmodel.NewsViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class NewsModule {
    @NewsScope // Limit view model to life cycle of view
    @Provides
    fun provideNewsViewModelFactory(
        getNewsUseCase: GetNewsUseCase,
        updateNewsUseCase: UpdateNewsUseCase
    ): NewsViewModelFactory = NewsViewModelFactory(getNewsUseCase, updateNewsUseCase)

}