package com.example.bloot_mohammadhosseinranjbar.presentation.di.core

import com.example.bloot_mohammadhosseinranjbar.data.api.NewsService
import com.example.bloot_mohammadhosseinranjbar.data.db.news.NewsDAO
import com.example.bloot_mohammadhosseinranjbar.data.repository.news.NewsCacheDataSourceImpl
import com.example.bloot_mohammadhosseinranjbar.data.repository.news.NewsLocalDataSourceImpl
import com.example.bloot_mohammadhosseinranjbar.data.repository.news.NewsRemoteDataSourceImpl
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsCacheDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsLocalDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRemoteDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteDataSourceModule {
    @Singleton
    @Provides
    fun provideNewsRemoteDataSource(newsService: NewsService): NewsRemoteDataSource =
        NewsRemoteDataSourceImpl(newsService)
}

@Module
class LocalDataSourceModule {
    @Singleton
    @Provides
    fun provideNewsLocalDataSource(newsDAO: NewsDAO): NewsLocalDataSource =
        NewsLocalDataSourceImpl(newsDAO)

}

@Module
class CacheDataSourceModule {
    @Singleton
    @Provides
    fun provideNewsLocalDataSource(): NewsCacheDataSource =
        NewsCacheDataSourceImpl()

}