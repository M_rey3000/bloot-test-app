package com.example.bloot_mohammadhosseinranjbar.domain.usecase.news

import com.example.bloot_mohammadhosseinranjbar.data.model.news.News
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRepository

class UpdateNewsUseCase(private val newsRepository: NewsRepository) {
    suspend fun execute(): List<News> = newsRepository.updateNews()
}