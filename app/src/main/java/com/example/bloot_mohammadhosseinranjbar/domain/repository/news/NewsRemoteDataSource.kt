package com.example.bloot_mohammadhosseinranjbar.domain.repository.news

import com.example.bloot_mohammadhosseinranjbar.data.model.news.NewsList
import retrofit2.Response

interface NewsRemoteDataSource {
    suspend fun getNews(): Response<NewsList>
}