package com.example.bloot_mohammadhosseinranjbar.domain.repository.news

import com.example.bloot_mohammadhosseinranjbar.data.model.news.News

interface NewsCacheDataSource {
    suspend fun getNewsFromCache(): List<News>
    suspend fun saveNewsToCache(newsLists: List<News>)
}