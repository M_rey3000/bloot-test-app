package com.example.bloot_mohammadhosseinranjbar.domain.repository.news

import com.example.bloot_mohammadhosseinranjbar.data.model.news.News

interface NewsRepository {
    suspend fun getNews(): List<News>
    suspend fun updateNews(): List<News>
}