package com.example.bloot_mohammadhosseinranjbar.domain.repository.news

import com.example.bloot_mohammadhosseinranjbar.data.model.news.News

interface NewsLocalDataSource {
    suspend fun getNewsFromDB(): List<News>
    suspend fun saveNewsToDB(news: List<News>)
    suspend fun clearAll()
}