package com.example.bloot_mohammadhosseinranjbar.data.model.news


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Source(
    @SerializedName("id")
    val sourceId: String?,
    @SerializedName("name")
    val name: String?
) : Parcelable