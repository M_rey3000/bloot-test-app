package com.example.bloot_mohammadhosseinranjbar.data.repository.news

import com.example.bloot_mohammadhosseinranjbar.data.db.news.NewsDAO
import com.example.bloot_mohammadhosseinranjbar.data.model.news.News
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsLocalDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsLocalDataSourceImpl(private val newsDAO: NewsDAO) : NewsLocalDataSource {

    // Room will use background thread for fetching data automatically
    override suspend fun getNewsFromDB(): List<News> = newsDAO.getAllNews()


    override suspend fun saveNewsToDB(news: List<News>) {
        CoroutineScope(Dispatchers.IO).launch {
            newsDAO.saveAllNews(news)
        }
    }

    override suspend fun clearAll() {
        CoroutineScope(Dispatchers.IO).launch {
            newsDAO.deleteAllNews()
        }
    }
}