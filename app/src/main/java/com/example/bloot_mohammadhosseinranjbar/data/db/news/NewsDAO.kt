package com.example.bloot_mohammadhosseinranjbar.data.db.news

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.bloot_mohammadhosseinranjbar.data.model.news.News

@Dao
interface NewsDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveAllNews(news: List<News>)

    @Query("SELECT * FROM news_table")
    suspend fun getAllNews(): List<News>

    @Query("DELETE FROM news_table")
    suspend fun deleteAllNews()
}