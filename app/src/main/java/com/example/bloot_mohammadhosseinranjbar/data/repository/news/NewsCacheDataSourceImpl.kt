package com.example.bloot_mohammadhosseinranjbar.data.repository.news

import com.example.bloot_mohammadhosseinranjbar.data.model.news.News
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsCacheDataSource

class NewsCacheDataSourceImpl : NewsCacheDataSource {

    private var newsList = ArrayList<News>()
    override suspend fun getNewsFromCache(): List<News> = newsList

    override suspend fun saveNewsToCache(news: List<News>) {
        newsList.clear()
        newsList = ArrayList(news)
    }
}