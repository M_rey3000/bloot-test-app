package com.example.bloot_mohammadhosseinranjbar.data.api

import com.example.bloot_mohammadhosseinranjbar.BuildConfig
import com.example.bloot_mohammadhosseinranjbar.data.model.news.NewsList
import com.google.gson.GsonBuilder
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query


interface NewsService {
    @GET("/v2/everything")
    suspend fun getNews(
        @Header("Authorization") api: String = BuildConfig.API_KEY,
//        @Query("pageSize") pageSize: Int = NewsRecycleViewAdapter.pageSize,
//        @Query("page") page: Int = NewsRecycleViewAdapter.pageToLoad,
        @Query("q") query: String = "keyword"
    ): Response<NewsList>
}

class RetrofitInstance {
    companion object {
        const val apiKey = "be31ee2da9a24ca29a35f1fedf80d96e"

        //private const val BASE_URL = "https://newsapi.org/"
        fun getRetrofitInstance(): Retrofit {
            return Retrofit.Builder()
                //.baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
        }
    }
}

