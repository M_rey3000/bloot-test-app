package com.example.bloot_mohammadhosseinranjbar.data.db.news

import android.content.Context
import androidx.room.*
import com.example.bloot_mohammadhosseinranjbar.data.model.news.News
import com.example.bloot_mohammadhosseinranjbar.data.model.news.Source

@Database(entities = [News::class], version = 1)
@TypeConverters(Converters::class)
abstract class NewsDB : RoomDatabase() {
    abstract val newsDAO: NewsDAO

    companion object {
        @Volatile // Made field visible immediately in other threads
        private var INSTANCE: NewsDB? = null
        fun getInstance(context: Context): NewsDB {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        NewsDB::class.java, "news_db"
                    ).build()
                }
                return instance
            }
        }
    }
}

class Converters {
    @TypeConverter
    fun fromSource(source: Source?): String? {
        return source?.let { source.name }
    }
}
