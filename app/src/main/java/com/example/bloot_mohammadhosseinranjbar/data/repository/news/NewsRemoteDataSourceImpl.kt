package com.example.bloot_mohammadhosseinranjbar.data.repository.news

import com.example.bloot_mohammadhosseinranjbar.data.api.NewsService
import com.example.bloot_mohammadhosseinranjbar.data.model.news.NewsList
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRemoteDataSource
import retrofit2.Response

class NewsRemoteDataSourceImpl(private val service: NewsService) : NewsRemoteDataSource {
    override suspend fun getNews(): Response<NewsList> = service.getNews()
}