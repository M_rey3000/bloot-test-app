package com.example.bloot_mohammadhosseinranjbar.data.repository.news

import android.util.Log
import com.example.bloot_mohammadhosseinranjbar.data.model.news.News
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsCacheDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsLocalDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRemoteDataSource
import com.example.bloot_mohammadhosseinranjbar.domain.repository.news.NewsRepository

class NewsRepositoryImpl(
    private val newsRemoteDataSource: NewsRemoteDataSource,
    private val newsLocalDataSource: NewsLocalDataSource,
    private val newsCacheDataSource: NewsCacheDataSource
) : NewsRepository {

    override suspend fun getNews(): List<News> = getNewsFromCache()

    override suspend fun updateNews(): List<News> {
        val newsFromApi = getNewsFromAPI()
        newsLocalDataSource.clearAll()
        newsLocalDataSource.saveNewsToDB(newsFromApi)
        newsCacheDataSource.saveNewsToCache(newsFromApi)
        return newsFromApi
    }

    private suspend fun getNewsFromAPI(): List<News> {
        var newsList: List<News> = listOf()
        try {
            val response = newsRemoteDataSource.getNews()
            val body = response.body()
            Log.d("RESPONSE", response.body().toString())
            if (body != null && response.isSuccessful) {
                newsList = body.articles!!
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return newsList
    }

    private suspend fun getNewsFromDB(): List<News> {
        var newsList: List<News> = listOf()
        try {
            newsList = newsLocalDataSource.getNewsFromDB()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (newsList.isNotEmpty())
            return newsList
        else {
            newsList = getNewsFromAPI()
            newsLocalDataSource.saveNewsToDB(newsList)
        }

        return newsList
    }

    private suspend fun getNewsFromCache(): List<News> {
        var newsList: List<News> = listOf()
        try {
            newsList = newsCacheDataSource.getNewsFromCache()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (newsList.isNotEmpty())
            return newsList
        else {
            newsList = getNewsFromDB()
            newsCacheDataSource.saveNewsToCache(newsList)
        }

        return newsList
    }
}