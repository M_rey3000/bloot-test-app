package com.example.bloot_mohammadhosseinranjbar.data.model.news


import com.google.gson.annotations.SerializedName

data class NewsList(
    @SerializedName("articles")
    val articles: List<News>?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("totalResults")
    val totalResults: Int?
)